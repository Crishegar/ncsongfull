

# --- !Ups

CREATE TABLE artist(username character varying(50) NOT NULL UNIQUE,	pass character varying(50) NOT NULL, name character varying(100) NOT NULL, lastname character varying(100) NOT NULL, CONSTRAINT PK_ARTIST PRIMARY KEY (username));

CREATE TABLE song(username_artist character varying(50), title character varying(50) NOT NULL, uri character varying (100)NOT NULL UNIQUE, CONSTRAINT PK_SONG PRIMARY KEY (uri), CONSTRAINT FK_SONG_ARTIST FOREIGN KEY (username_artist) REFERENCES artist(username));

# --- !Downs

DROP TABLE IF EXISTS song, artist CASCADE;