package models;

import static org.junit.Assert.*;
import org.junit.Test;
import models.*;

public class SongTest{

    @Test
    public void testGets(){
        Song song = new Song("title", "uri", new Artist("username", "password", "name", "lastname"));
        assertEquals("El title deberia ser : title",song.getTitle(),"title");
        assertEquals("El name deberia ser : name",song.getUri(), "uri");
        assertEquals("El artist deberia ser : asi Artista: name=name lastname=lastname", song.getArtist().toString(),"Artista: name=name lastname=lastname");
    }

    @Test
    public void testSets(){
        Song song = new Song();
        song.setTitle("title");
        song.setUri("uri");
        song.setArtist(new Artist("username", "password", "name", "lastname"));
        assertEquals("El title deberia ser : title",song.getTitle(),"title");
        assertEquals("El name deberia ser : name",song.getUri(), "uri");
        assertEquals("El artist deberia ser : asi Artista: name=name lastname=lastname", song.getArtist().toString(),"Artista: name=name lastname=lastname");

    }
}