package controllers;

import models.*;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import javax.inject.Inject;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;
import javax.persistence.*;
import play.db.jpa.JPAApi;
import java.util.List;
import play.mvc.*;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import com.cloudinary.*;
import com.cloudinary.utils.ObjectUtils;
import java.io.File;
import java.util.Map;


public class SongController extends Controller{

    @Inject
    FormFactory fm;

    @Inject
    JPAApi api;

    /**
     * MEtodo que se encarga de seleccionar una Cancion y subirlo a la base de datos de la applicacion
     * @param username El username del usuario que sube la Cancion
     * @return de ser exitosa la subida del archivo, redirecciona al usuario hacia los datos de la cuenta,
     * de lo contrario envia mensaje de error
     */
    @Transactional
    public Result upload(String username){
        MultipartFormData<File> body = request().body().asMultipartFormData();
        FilePart <File> songFilePart = body.getFile("song");
        if(songFilePart != null){
            String fileName = songFilePart.getFilename();
            if(fileName.endsWith(".mp3") || fileName.endsWith(".MP3")){
                String contentType = songFilePart.getContentType();
                File file = songFilePart.getFile();
                Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                        "cloud_name", "crishegar",
                        "api_key", "321276547173215",
                        "api_secret", "z4GZBATt5q7NFB-IwQobDr2Slcc"));
                try{
                    Map uploadResult = cloudinary.uploader().upload(file, ObjectUtils.asMap(
                            "resource_type","auto"
                    ));
                    EntityManager em = api.em();
                    Artist artist = em.find(Artist.class, username);
                    Song cancion = new Song(fileName, uploadResult.get("url").toString(), artist);
                    em.persist(cancion);
                    return redirect(controllers.routes.ArtistController.show(username));
                }catch(Exception e){
                    e.printStackTrace();
                    return badRequest(e.getMessage());
                }
            }else{
                return badRequest(uploadsong.render(username,"Formato no valido"));
            }
        }else{
            return badRequest(uploadsong.render(username,"Archivo inexistente"));
        }
    }

    /**
     * Metodo que se encarga de eliminar una cancion
     * @param uri Direccion en la cual se encuentra la cancion a eliminar
     * @param username Usuario quue desea eliminar una cancion
     * @return de ser exitosa la eliminacion envia al usuario a visualizar los datos de su cuenta,
     * de lo contrario envia mensaje de error
     */
    @Transactional
    public Result delete(String uri, String username){
        EntityManager em = api.em();
        Song song = em.find(Song.class, uri);
        if(song !=null){
            em.remove(song);
            return redirect(controllers.routes.ArtistController.show(username));
        }else{
            return badRequest("La cancion no existe");
        }
    }

    /**
     * Metodo que se encarga de buscar todas las canciones existentes en la base de datos
     * @return Genera la pagina de inicio, la cual muestra todas las canciones.
     */
    @Transactional(readOnly=true)
    public Result index(){
        EntityManager em = api.em();
        TypedQuery <Song> q = em.createQuery("SELECT s FROM Song s", Song.class);
        List <Song> songs = q.getResultList();
        return ok(index.render(songs));
    }

}