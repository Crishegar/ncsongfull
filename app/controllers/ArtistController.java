package controllers;

import models.*;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import javax.inject.Inject;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;
import javax.persistence.*;
import play.db.jpa.JPAApi;
import java.util.List;

public class ArtistController extends Controller {

    /**
     * Inject the Factory necesario para crear Formularios
     */
    @Inject
    FormFactory fm;

    /**
     * Inject the JPAApi para poder usar el EntityManager
     */
    @Inject
    JPAApi api;

    public Result create(){
        Form <Artist> form = fm.form(Artist.class).bindFromRequest();
        return ok(sign_in.render(form));
    }

    /**
     * Metodo que se encarga de Ingresar los datos del artista, y hacerlo persistir
     * @return Redirecciona hacia los datos de la cuenta del artista, o retorna un mensaje de error
     */
    @Transactional
    public Result addArtist() {
        EntityManager em = api.em();
        Form<Artist> form = fm.form(Artist.class).bindFromRequest();
        Artist art = form.get();
        List<Artist> artistas = listArtistas();
        for (Artist a: artistas){
            if(a.getUsername().equals(art.getUsername())){
                form.reject("username","El usuario ya existe");
                return badRequest(sign_in.render(form));
            }
        }
        em.persist(art);
        return redirect(routes.ArtistController.show(art.getUsername()));
    }

    /**
     * Metodo que se encarga de mostrar los datos de la cuenta de un artista.
     * @param username El username del artista
     * @return Muestra los datos del artista existente, si no Envia un mensaje de error
     */
    @Transactional(readOnly=true)
    public Result show(String username) {
        Artist artist = api.em().find(Artist.class,username);
        if(artist!=null){
            return ok(logs.render(artist));
        }else{
            return badRequest("el artista no existe");
        }
    }

    /**
     * Metodo que se encarga de cargar los nuevos datos del artista
     * @param username el username del artista que desea cambiar sus datos de cuenta
     * @return Redirecciona hacia los datos de la cuenta del artista con los cambios, o envia un mensaje de error
     */
    @Transactional
    public Result update(String username) {
        EntityManager em = api.em();
        Form<Artist> form = fm.form(Artist.class).bindFromRequest();
        java.util.Map<String, String> hm = form.data();
        try {
            Artist artist = em.find(Artist.class, username);
            artist.setPass(hm.get("pass"));
            artist.setName(hm.get("name"));
            artist.setLastname(hm.get("lastname"));
            return redirect(controllers.routes.ArtistController.show(artist.getUsername()));
        } catch (Exception e) {
            form.reject("username", "user already exist");
            return badRequest("mal");
        }

    }

    /**
     * MEtodo que se encarga de crear el formulario de edicion de los datos de la cuenta del artista
     * @param username El username del artista que desea realizar los cambios
     * @return Visualiza el formulario a llenar para cambiar los datos de una cuenta, o envia un mensaje de error
     * si el artista no existe
     */
    @Transactional
    public Result edit(String username){
        EntityManager em =api.em();
        Artist artist = em.find(Artist.class, username);
        if(artist != null){
            Form<Artist> form = fm.form(Artist.class).bindFromRequest();
            return ok(edit.render(form,artist));
        }else{
            return badRequest("el artista no existe");
        }
   }

    /**
     * Metodo que se encarga de eliminar la cuenta de un artista.
     * @param username El username de la cuenta que va a ser Eliminada
     * @return Mensaje de exito si la cuenta es eliminada, si no envia mensaje de error
     */
    @Transactional
    public Result delete(String username){
        EntityManager em = api.em();
        Artist artist = em.find(Artist.class, username);
        if(artist !=null){
            if(artist.getSongs().size()!=0){
                return show(username);
            }else{
                em.remove(artist);
                SongController sc = new SongController();
                return search();
            }
        }else{
            return badRequest("La cancion no existe");
        }
    }

    /**
     * Metodo que se encarga de crear el formulario para la busqueda de un artista, por su username
     * @return El formulario de busqueda
     */
    public Result search(){
        Form<Artist> form = fm.form(Artist.class).bindFromRequest();
        return ok(search.render(form));
    }

    /**
     * Metodo que se encarga de obtener los datos del formulario de busqueda, para poder buscar el artista
     * @return Redirecciona hacia la pagina de la cuenta del artista buscado, o envia un mensaje de error si el artista
     * no existe
     */
    @Transactional
    public Result buscar(){
        EntityManager em = api.em();
        Form<Artist> form = fm.form(Artist.class).bindFromRequest();
        Artist art = form.get();
        List<Artist> artistas = listArtistas();
        if(art != null){
            for(Artist artista : artistas){
                if(artista.getUsername().equals(art.getUsername())){
                    return redirect(controllers.routes.ArtistController.show(art.getUsername()));
                }
            }
        }else{
            form.reject("username","El artista no existe!!!");
            return badRequest(search.render(form));
        }
        form.reject("username","El artista no existe!!!");
        return badRequest(search.render(form));
    }

    /**
     * MEtodo que se encarga de crear el formulario para agregar una cancion a la cuenta del artista
     * @param username el username del artista que desea agregar la cancion
     * @return Genera el Formulario para subir una cancion
     */
    @Transactional
    public Result uploadSong(String username){

        return ok(uploadsong.render(username,""));
    }

    
    @Transactional (readOnly=true)
    public List listArtistas(){
        EntityManager em = api.em();
        TypedQuery <Artist> sql = em.createQuery("SELECT a FROM Artist a", Artist.class);
        List<Artist> artists = sql.getResultList();
        return artists;
    }
}
